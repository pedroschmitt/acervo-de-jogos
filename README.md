# README

Sistema para controle de aluguéis de boardgames da empresa Ludoteca.

Painel para equipe: acervo.ludoteca.com.br/admin

docker-compose build
docker-compose up
docker-compose run --rm app bundle exec rails db:create db:migrate db:seed
