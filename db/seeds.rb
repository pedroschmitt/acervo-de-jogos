# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Company.find_or_create_by(name: "Exemplo", email: "exemplo@exemplo.com.br", shopname: "Exemplo", subdomain: "exemplo")

company = Company.last
User.create name: company.name, status: :active, kind: :manager, email: company.email, password: '123456'
Shop.create name_for_title: company.shopname, theme_color: 0
#email: company.email


#Apartment::Tenant.create('tenant_name')

#User.create name: "Pedro", status: :active, kind: :manager, email: "pedromartins.bsb@gmail.com", password: '123456'

Situation.create name: 'Disponível'
Situation.create name: 'Alugado'
Situation.create name: 'Manutenção'

Category.create name: 'Azul',    days: 7, rentable: true
Category.create name: 'Amarelo', days: 7, rentable: true
Category.create name: 'Dourado', days: 14, rentable: true
Category.create name: 'Rosa',    days: 0

# Criando alguns jogos de exemplo

Plan.create name: "Nivel 1", value: "80"
Plan.create name: "Nivel 2", value: "120"
Plan.create name: "Nivel 3", value: "140"
Plan.create name: "Avulso"

# Criando alguns jogos de exemplo
Boardgame.create name: "20th Century", copy: 0, category_id: 2, situation_id: 1, remote_image_url: "https://www.ludopedia.com.br/images/jogos/capas/2628_t.jpg"
Boardgame.create name: "4 Gods", copy: 0, category_id: 2, situation_id: 1, remote_image_url: "https://www.ludopedia.com.br/images/jogos/capas/9348_m.jpg"
Boardgame.create name: "7 Wonders", copy: 0, category_id: 2, situation_id: 1, remote_image_url: "https://www.ludopedia.com.br/images/jogos/capas/8_m.jpg"
Boardgame.create name: "7 Wonders", copy: 1, category_id: 2, situation_id: 2, remote_image_url: "https://www.ludopedia.com.br/images/jogos/capas/8_m.jpg"
Boardgame.create name: "7 Wonders", copy: 2, category_id: 2, situation_id: 2, remote_image_url: "https://www.ludopedia.com.br/images/jogos/capas/8_m.jpg"
