class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.references :client, foreign_key: true
      t.references :plan, foreign_key: true
      t.date :subscription_date
      t.date :subscription_cancelation_date
      t.integer :status
      t.text :notes

      t.timestamps
    end
  end
end
