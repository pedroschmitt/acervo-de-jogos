class AddDaysToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :days, :integer
  end
end
