class AddBirthdayToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :birthday, :datetime
  end
end
