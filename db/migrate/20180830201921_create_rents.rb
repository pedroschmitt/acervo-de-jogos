class CreateRents < ActiveRecord::Migration[5.2]
  def change
    create_table :rents do |t|
      t.references :client, foreign_key: true
      t.references :boardgame, foreign_key: true
      t.references :plan, foreign_key: true
      t.date :rent_date
      t.date :expected_return_date
      t.references :user, foreign_key: true
      t.text :notes
      t.date :returned_date
      t.integer :status

      t.timestamps
    end
  end
end
