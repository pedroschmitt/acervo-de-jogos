class CreateShops < ActiveRecord::Migration[5.2]
  def change
    create_table :shops do |t|
      t.string :name_for_title
      t.string :theme_color
      t.string :company_name
      t.string :address
      t.string :cnpj
      t.string :phone
      t.string :whatsapp
      t.string :logo
      t.string :site
      t.string :email
      t.string :facebook_link
      t.string :twitter_link
      t.string :instagram_link

      t.timestamps
    end
  end
end
