class AddBggToBoardgames < ActiveRecord::Migration[5.2]
  def change
    add_column :boardgames, :idbgg, :integer
    add_column :boardgames, :min_players, :integer
    add_column :boardgames, :max_players, :integer
    add_column :boardgames, :minimum_age, :integer
    add_column :boardgames, :playing_time, :integer
    add_column :boardgames, :bayesian_average, :float
    add_column :boardgames, :average_rating, :float
  end
end
