class Boardgame < ApplicationRecord
  # enum copy: [:copy_1, :copy_2, :copy_3, :copy_4, :copy_5]
  enum copy: ['cópia 1', 'cópia 2', 'cópia 3', 'cópia 4', 'cópia 5']
  belongs_to :category
  belongs_to :situation
  has_many :rents
  has_paper_trail
  
  mount_uploader :image, ImageUploader

  after_create :update_bggbase


  # Funcao para a api do bgg nao trazer todos os nomes, de varias linguas.
  def self.primary_name(name_array)
    name_array.each do |name|
      return name["value"] if name["type"] == "primary"
    end
  end

  def update_bggbase
    if idbgg.present?
      bgg = BggApi.new
      bgg_data = bgg.thing(id: idbgg,
                                stats: 1)["item"].first

      self.name             = Boardgame.primary_name(bgg_data["name"])
      self.remote_image_url = bgg_data["image"].first if bgg_data["image"]
      self.min_players      = bgg_data["minplayers"].first["value"]
      self.max_players      = bgg_data["maxplayers"].first["value"]
      self.playing_time     = bgg_data["playingtime"].first["value"]
      self.minimum_age      = bgg_data["minage"].first["value"]
      self.average_rating   = bgg_data["statistics"].first["ratings"].first["average"].first["value"]
      self.bayesian_average = bgg_data["statistics"].first["ratings"].first["bayesaverage"].first["value"]

      self.save
    end
  end

end
