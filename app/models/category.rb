class Category < ApplicationRecord
  has_many :boardgames
  validates :days, presence: true
end
