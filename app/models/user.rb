class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :timeoutable
  enum kind: [:employee, :manager]
  enum status: [:active, :inactive]
  has_many :rents
  has_many :subscriptions
end
