class Client < ApplicationRecord
  enum status: [:active, :inactive]
  has_many :rents
  has_one :subscription
  validates :name, :document, :email, :phone, :address, :birthday, presence: true
  has_paper_trail
  
  
  mount_uploader :documentphoto, DocumentphotoUploader
  
  # before_create do
  #   write_attribute(:document, document.to_cpf_format)
  # end
  
  before_save do
    if !CpfUtils.valid_cpf?(document)
      throw :abort
    end
    write_attribute(:document, document.to_cpf_format)
  end
  
end
