Rails.application.routes.draw do
  devise_for :users
  resources :boardgames
  root 'boardgames#index'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # root 'admin#dashboard'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
